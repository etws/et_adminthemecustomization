== Extension links
Permanent link: http://shop.etwebsolutions.com/rus/et-adminthemecustomization.html
Support link: http://support.etwebsolutions.com/projects/et-adminthemecustomization/roadmap

== Short Description
Добавляет возможность
* скрывать неиспользуемые языки в панели адмиинистрирования;
* указывать своё название темы для панели администрирования; После этого вы можете создавать свою тему для панели администрирования, не меняя оригинальных файлов.


== Version Compatibility
Magento CE:
1.6.x (tested in 1.6.1.0)
1.7.x (tested in 1.7.0.2)
1.8.x (tested in 1.8.0.0)

== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* If you have downloaded it, copy all files from the "install" folder to the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> ET EXTENSIONS -> Admin Theme Customization)
* Run the compilation process and enable cache if needed
