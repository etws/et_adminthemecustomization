<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_AdminThemeCustomization
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

class ET_AdminThemeCustomization_Model_Observer
{
    //Event: adminhtml_controller_action_predispatch_start
    public function overrideTheme()
    {
        /** @var $helper ET_AdminThemeCustomization_Helper_Data */
        $helper = Mage::helper('et_adminthemecustomization');
        if ($helper->isOverrideEnabled()) {
            Mage::getDesign()->setArea('adminhtml')->setTheme($helper->getAdminThemeName());
        }
    }
}