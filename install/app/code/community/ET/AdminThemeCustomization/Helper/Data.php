<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_AdminThemeCustomization
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

class ET_AdminThemeCustomization_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function hideUnusedTranslations()
    {
        return Mage::getStoreConfig('et_adminthemecustomization/translation/hide_unused_translation');
    }

    public function getSelectedLanguages()
    {
        $languages = array();
        $storedValues = Mage::getStoreConfig('et_adminthemecustomization/translation/show_only');
        if (strlen($storedValues) == 0) {
            $languages = Mage::app()->getLocale()->getOptionLocales();
        } else {
            $storedValues = explode(',', $storedValues);
            $allLanguages = Mage::app()->getLocale()->getOptionLocales();
            foreach ($allLanguages as $key => $value) {
                if (in_array($value['value'], $storedValues)) {
                    $languages[] = $value;
                }
            }
        }

        return $languages;
    }

    public function getLanguagesEnabledForTranslation()
    {
        return $this->getSelectedLanguages();
    }

    public function isOverrideEnabled()
    {
        $themeName = $this->getAdminThemeName();
        return Mage::getStoreConfig('design/admin/override_theme') && (strlen($themeName) > 0);
    }

    public function getAdminThemeName()
    {
        return (string)Mage::getStoreConfig('design/admin/theme_name');
    }
}