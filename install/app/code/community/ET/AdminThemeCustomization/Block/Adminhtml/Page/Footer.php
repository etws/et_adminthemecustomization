<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_AdminThemeCustomization
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

class ET_AdminThemeCustomization_Block_Adminhtml_Page_Footer extends Mage_Adminhtml_Block_Page_Footer
{
    /**
     * Function rewritten for hiding unused languages in admin footer
     *
     * @return mixed
     */
    public function getLanguageSelect()
    {
        /** @var $helper ET_AdminThemeCustomization_Helper_Data */
        $helper = Mage::helper('et_adminthemecustomization');
        if ($helper->hideUnusedTranslations()) {
            $locale = Mage::app()->getLocale();
            $cacheId = self::LOCALE_CACHE_KEY . $locale->getLocaleCode();
            $html = Mage::app()->loadCache($cacheId);
            if (!$html) {
                $html = $this->getLayout()->createBlock('adminhtml/html_select')
                    ->setName('locale')
                    ->setId('interface_locale')
                    ->setTitle(Mage::helper('page')->__('Interface Language'))
                    ->setExtraParams('style="width:200px"')
                    ->setValue($locale->getLocaleCode())
                    //->setOptions($locale->getTranslatedOptionLocales())
                    ->setOptions($helper->getLanguagesEnabledForTranslation())
                    ->getHtml();
                Mage::app()->saveCache($html, $cacheId, array(self::LOCALE_CACHE_TAG), self::LOCALE_CACHE_LIFETIME);
            }
        } else {
            $html = parent::getLanguageSelect();
        }

        return $html;
    }


}